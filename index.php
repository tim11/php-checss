<?php
function convert_char_to_col(string $char): int {
    return ord($char) - 64;
}
function convert_col_to_char(int $col): string {
    if ($col < 1 || $col > 8) {
        return '';
    }
    return chr(64 + $col);
}
interface IFigure {
    public function get_name(): string;
    public function get_color(): int;
    public function can_move(IBoard $board, int $from_col, int $from_row, int $to_col, int $to_row): bool;
    public function can_attack(IBoard $board, int $from_col, int $from_row, int $to_col, int $to_row): bool;
}

abstract class BFigure implements IFigure
{
    const BLACK = 0;
    const WHITE = 1;
    protected string $name;
    protected int $color;
    public function __construct(int $color) {
        $this->color = $color;
    }
    public function get_name(): string {
        return $this->name;
    }

    public function get_color(): int {
        return $this->color;
    }
    public function can_move(IBoard $board, int $from_col, int $from_row, int $to_col, int $to_row): bool {
        return $board->check_position($from_col, $from_row) && $board->check_position($to_col, $to_row);
    }

    public function can_attack(IBoard $board, int $from_col, int $from_row, int $to_col, int $to_row): bool {
        return $this->can_move($board, $from_col, $from_row, $to_col, $to_row);
    }
}

/**
 * Figures
 */
abstract class Pawn extends BFigure
{
    public function __construct(int $color) {
        $this->name = $color == self::WHITE ? "\u{2659}" : "\u{265F}";
        parent::__construct($color);
    }

    public function can_move(IBoard $board, int $from_col, int $from_row, int $to_col, int $to_row): bool {
        if (!parent::can_move($board, $from_col, $from_row, $to_col, $to_row))
            return false;
        if ($from_col != $to_col) {
            return false;
        }
        if ($this->color == self::WHITE) {
            $direction = 1;
            $start_row = 2;
        } else {
            $direction = -1;
            $start_row = 7;
        }
        if ($from_row + $direction == $to_row) {
            return true;
        }
        if (
            $from_row == $start_row &&
            $from_row + (2 * $direction) == $to_row &&
            !$board->get_figure($from_col, $from_row + $direction)
        ){
            return true;
        }
        return false;
    }

    public function can_attack(IBoard $board, int $from_col, int $from_row, int $to_col, int $to_row): bool {
        $direction = $this->color == self::WHITE ? 1 : -1;
        $difference = abs($from_col - $to_col);
        return $from_row + $direction == $to_row && $difference == 1;
    }
}

abstract class Rook extends BFigure {
    public function __construct(int $color) {
        $this->name = $color == self::WHITE ? "\u{2656}" : "\u{265C}";
        parent::__construct($color);
    }

    public function can_move(IBoard $board, int $from_col, int $from_row, int $to_col, int $to_row): bool {
        if (!parent::can_move($board, $from_col, $from_row, $to_col, $to_row))
            return false;
        $row_d = abs($to_row - $from_row);
        $col_d = abs($to_col - $from_col);
        if ($row_d != 0 && $col_d != 0)
            return false;
        if ($row_d == 1 || $col_d == 1)
            return true;
        if ($row_d) {
            $step = $to_row >= $from_row ? 1 : -1;
            foreach (range($from_row + $step, $to_row - $step, $step) as $row) {
                $figure = $board->get_figure($from_col, $row);
                if ($figure)
                    return false;
            }
        }
        if ($col_d) {
            $step = $to_col >= $from_col ? 1 : -1;
            foreach (range($from_col + $step, $to_col - $step, $step) as $col) {
                $figure = $board->get_figure($col, $from_row);
                if ($figure)
                    return false;
            }
        }
        return true;
    }
}
abstract class Bishop extends BFigure {
    public function __construct(int $color) {
        $this->name = $color == self::WHITE ? "\u{2657}" : "\u{265D}";
        parent::__construct($color);
    }

    public function can_move(IBoard $board, int $from_col, int $from_row, int $to_col, int $to_row): bool
    {
        if (!parent::can_move($board, $from_col, $from_row, $to_col, $to_row))
            return false;
        $col_d = abs($from_col - $to_col);
        $row_d = abs($from_row - $to_row);
        if ($col_d != $row_d)
            return false;
        if ($col_d == 1)
            return true;
        $step_col = $to_col >= $from_col ? 1 : -1;
        $step_row = $to_row >= $from_row ? 1 : -1;
        $col =  $from_col + $step_col;
        $row =  $from_row + $step_row;
        while ($col != $to_col && $row != $to_row) {
            $figure = $board->get_figure($col, $row);
            if ($figure)
                return false;
            $col += $step_col;
            $row += $step_row;
        }
        return true;
    }
}
abstract class Knight extends BFigure {
    public function __construct(int $color) {
        $this->name = $color == self::WHITE ? "\u{2658}" : "\u{265E}";
        parent::__construct($color);
    }

    public function can_move(IBoard $board, int $from_col, int $from_row, int $to_col, int $to_row): bool {
        if (!parent::can_move($board, $from_col, $from_row, $to_col, $to_row))
            return false;
        $col_d = abs($from_col - $to_col);
        $row_d = abs($from_row - $to_row);
        return $col_d == 1 && $row_d == 2 || $col_d == 2 && $row_d == 1;
    }
}
abstract class Queen extends BFigure {
    public function __construct(int $color) {
        $this->name = $color == self::WHITE ? "\u{2655}" : "\u{265B}";
        parent::__construct($color);
    }

    public function can_move(IBoard $board, int $from_col, int $from_row, int $to_col, int $to_row): bool {
        if (!parent::can_move($board, $from_col, $from_row, $to_col, $to_row))
            return false;
        return true;
    }
}
abstract class King extends BFigure {
    public function __construct(int $color) {
        $this->name = $color == self::WHITE ? "\u{2654}" : "\u{265A}";
        parent::__construct($color);
    }

    public function can_move(IBoard $board, int $from_col, int $from_row, int $to_col, int $to_row): bool {
        if (!parent::can_move($board, $from_col, $from_row, $to_col, $to_row))
            return false;
        return true;
    }
}



final class WoodPawn extends Pawn {}
final class WoodRook extends Rook {}
final class WoodKnight extends Knight {}
final class WoodBishop extends Bishop {}
final class WoodQueen extends Queen {}
final class WoodKing extends King {}
final class StonePawn extends Pawn {}
final class StoneRook extends Rook {}
final class StoneKnight extends Knight {}
final class StoneBishop extends Bishop {}
final class StoneQueen extends Queen {}
final class StoneKing extends King {}

interface IBoard
{
    public function add_figure(int $col, int $row, IFigure $figure): void;
    public function print(): void;
    public function get_figure(int $col, int $row): IFigure | null;
    public function check_position(int $col, int $row): bool;
    public function move(int $from_col, int $from_row, int $to_col, int $to_row): string;
}

abstract class BBoard implements IBoard
{
    public int $gamer = BFigure::WHITE;
    protected string $pawn = Pawn::class;
    protected string $rook = Rook::class;
    protected string $knight = Knight::class;
    protected string $bishop = Bishop::class;
    protected string $queen = Queen::class;
    protected string $king = King::class;
    public array $field;
    public function __construct() {
        $this->field = [];
        for ($i = 0; $i < 8; $i++) {
            $row = [];
            for ($j = 0; $j < 8; $j++)
            {
                $row[] = null;
            }
            $this->field[] = $row;
        }
        foreach ([1, 8] as $index) {
            $this->add_figure($index, 1, new $this->rook(BFigure::WHITE));
            $this->add_figure($index, 8, new $this->rook(BFigure::BLACK));
            $this->add_figure(4, $index, new $this->queen(BFigure::WHITE));
            $this->add_figure(5, $index, new $this->king(BFigure::WHITE));
        }
        foreach ([2, 7] as $col) {
            $this->add_figure($col, 1, new $this->knight(BFigure::WHITE));
            $this->add_figure($col, 8, new $this->knight(BFigure::BLACK));
        }
        foreach ([3, 6] as $col) {
            $this->add_figure($col, 1, new $this->bishop(BFigure::WHITE));
            $this->add_figure($col, 8, new $this->bishop(BFigure::BLACK));
        }
        for ($col = 1; $col <= 8; $col++) {
            $this->add_figure($col, 2, new $this->pawn(BFigure::WHITE));
            $this->add_figure($col, 7, new $this->pawn(BFigure::BLACK));
        }
    }

    public function check_position(int $col, int $row): bool {
        return !($col < 1 || $col > 8 || $row < 1 || $row > 8);
    }
    public function add_figure(int $col, int $row, IFigure $figure): void
    {
        if ($this->check_position($col, $row)) {
            $this->field[--$row][--$col] = $figure;
        }
    }

    public function get_figure(int $col, int $row): IFigure | null {
        if (!$this->check_position($col, $row)) {
            return null;
        }
        return $this->field[--$row][--$col];
    }
    public function print(): void
    {
        for ($row = 8; $row > 0; $row--)
        {
            echo '  ' . str_repeat('-', 33) . PHP_EOL;
            echo $row . ' ';
            for ($col = 1; $col <= 8; $col++)
            {
                echo '| ';
                $name = ' ';
                $figure = $this->get_figure($col, $row);
                if ($figure)
                {
                    $name = $figure->get_name();
                }
                echo $name;
                echo ' ';
                if ($col == 8)
                {
                    echo '|';
                }
            }
            echo PHP_EOL;
            if ($row == 1)
            {
                echo '  ' . str_repeat('-', 33) . PHP_EOL;
            }
        }
        echo '  ';
        for ($col = 1; $col <= 8; $col++)
        {
            echo '  ';
            echo convert_col_to_char($col);
            echo ' ';
        }
    }

    public function move(int $from_col, int $from_row, int $to_col, int $to_row): string
    {
        if (!$this->check_position($from_col, $from_row) || !$this->check_position($to_col, $to_row)) {
            return 'Incorrect coordinates';
        }
        $figure = $this->get_figure($from_col, $from_row);
        if (!$figure) {
            return 'Figure not found';
        }
        if ($figure->get_color() != $this->gamer) {
            return 'It is not your figure';
        }
        if ($from_col == $to_col && $from_row == $to_row) {
            return 'You did not make a move';
        }
        $destination = $this->get_figure($to_col, $to_row);
        if (!$destination) {
            if (!$figure->can_move($this, $from_col, $from_row, $to_col, $to_row)) {
                return 'You can not go like that';
            }
        } else if ($figure->get_color() == $destination->get_color()) {
            return 'You can not go like that';
        } else if (!$figure->can_attack($this, $from_col, $from_row, $to_col, $to_row)) {
            return 'You can not go like that';
        }
        $this->field[$to_row - 1][$to_col - 1] = clone($figure);
        $this->field[$from_row - 1][$from_col - 1] = null;
        $this->gamer = $this->gamer == BFigure::WHITE ? BFigure::BLACK : BFigure::WHITE;
        return '';
    }
}

final class WoodBoard extends BBoard {
    public function __construct() {
        $this->pawn = WoodPawn::class;
        $this->rook = WoodRook::class;
        $this->knight = WoodKnight::class;
        $this->bishop = WoodBishop::class;
        $this->queen = WoodQueen::class;
        $this->king = WoodKing::class;
        parent::__construct();
    }
}
final class StoneBoard extends BBoard {
    public function __construct()
    {
        $this->pawn = StonePawn::class;
        $this->rook = StoneRook::class;
        $this->knight = StoneKnight::class;
        $this->bishop = StoneBishop::class;
        $this->queen = StoneQueen::class;
        $this->king = StoneKing::class;
        parent::__construct();
    }
}
final class BoardDirector {
    public static function generate(string $material): IBoard {
        switch ($material) {
            case 'stone':
                return new StoneBoard();
            default:
                return new WoodBoard();
        }
    }
}


$board = null;
$message = null;

while (true) {
    if ($board) {
        $board->print();
        echo PHP_EOL . str_repeat('=', 40) . PHP_EOL;
        if ($message) {
            echo "\x1b[37;41m    {$message}    \x1b[0m" . PHP_EOL;
        }
        echo 'Gamer: ';
        if ($board->gamer == BFigure::WHITE) {
            echo 'white';
        } else {
            echo 'black';
        }
        echo PHP_EOL;
    }
    echo 'Description:' . PHP_EOL;
    echo '- new: create new game' . PHP_EOL;
    echo '- exit: exit from game' . PHP_EOL;
    echo '- move <from> <to>: move figure in board' . PHP_EOL;
    echo 'Enter command: ';
    $command = trim(fgets(STDIN));
    switch ($command) {
        case 'exit':
            exit(0);
        case 'new':
            $board = BoardDirector::generate('wood');
            $message = null;
            break;
        default:
            $regexp = '/^move ([A-H])([1-8]) ([A-H])([1-8])$/';
            if (!preg_match($regexp, $command, $find)) {
                echo 'Unknown command' . PHP_EOL;
                break;
            }
            $from_row = (int) $find[2];
            $from_col = convert_char_to_col($find[1]);
            $to_row = (int) $find[4];
            $to_col = convert_char_to_col($find[3]);
            $message = $board->move($from_col, $from_row, $to_col, $to_row);
    }
}